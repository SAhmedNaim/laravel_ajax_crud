<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Laravel AJAX CRUD</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

	<nav class="navbar navbar-default navbar-ststic-top">
		<div class="container">
			<div class="navbar-header">
				<a href="{{ url('/post') }}" class="navbar-brand">Laravel AJAX CRUD</a>
			</div>
		</div>
	</nav>

	<div class="container">
		@yield('content')
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script type="text/javascript">
		
		$(document).on('click', '.create-modal', function(e){
			$('#create').modal('show');
			$('.form-horizontal').show();
			$('.modal-title').text('Add Post');
		});
		
	</script>

	<script type="text/javascript">
		$('#add').click(function(){
			$.ajax({
				type	: 'POST',
				url		: 'addPost',
				data	: {
					'_token'	: $("input[name=_token]").val(),
					'title'		: $("input[name=title]").val(),
					'body'		: $("input[name=body]").val(),
				},
				success	: function(data) {
					if((data.errors)) {
						$('.error').removeClass('hidden');
						$('.error').text(data.errors.title);
						$('.error').text(data.errors.body);
					} else {
						$('.error').remove();
						$('#table').append("<tr class='post" + data.id + "'>" +
							"<td>" + data.id + "</td>" + 
							"<td>" + data.title + "</td>" + 
							"<td>" + data.body + "</td>" + 
							"<td>" + data.created_at + "</td>" + 
							"<td><a class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>" + 
							"<i class='fa fa-eye'></i></a>" + 
							"<a class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>" + 
							"<i class='glyphicon glyphicon-pencil'></i></a>" + 
							"<a class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>" + 
							"<i class='glyphicon glyphicon-trash'></i></a>" + 
							"</td>" + 
							"</tr>"
							);
					}
				},
			});
			$('#title').val('');
			$('#body').val('');
		});
	</script>
	
	{{-- Show --}}
	<script type="text/javascript">
		$(document).on('click', '.show-modal', function(){
			$('#show').modal('show');
			$('.modal-title').text('Show Post');
		});
	</script>

	{{-- Edit --}}
	<script type="text/javascript">
		$(document).on('click', '.edit-modal', function(){
			$('#footer_action_button').text('Update Post');
			$('#footer_action_button').addClass('glyphicon-check');
			$('#footer_action_button').removeClass('glyphicon-trash');
			$('.actionBtn').addClass('btn-success');
			$('.actionBtn').removeClass('btn-danger');
			$('.actionBtn').addClass('edit');
			$('.modal-title').text('Post Edit');
			$('.deleteContent').hide();
			$('.form-horizontal').show();
			$('#fid').val($(this).data('id'));
			$('#t').val($(this).data('title'));
			$('#b').val($(this).data('body'));
			$('#myModal').modal('show');
		});

		$('.modal-footer').on('click', '.edit', function(){
			$.ajax({
				type	: 'POST',
				url		: 'editPost',
				data	: {
					'_token'	: $("input[name=_token]").val(),
					'id'		: $('#fid').val(),
					'title'		: $("#t").val(),
					'body'		: $("#b").val(),
				},
				success : function(data){
					$('.post' + data.id).replaceWith(" " + 
						"<tr class='post" + data.id + "'>" +
							"<td>" + data.id + "</td>" + 
							"<td>" + data.title + "</td>" + 
							"<td>" + data.body + "</td>" + 
							"<td>" + data.created_at + "</td>" + 
							"<td><a class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>" + 
							"<i class='fa fa-eye'></i></a>" + 
							"<a class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>" + 
							"<i class='glyphicon glyphicon-pencil'></i></a>" + 
							"<a class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>" + 
							"<i class='glyphicon glyphicon-trash'></i></a>" + 
							"</td>" + 
						"</tr>"
					);
				}
			});
		});

	</script>

	{{-- Delete --}}
	<script type="text/javascript">
		$(document).on('click', '.delete-modal', function(){
			$('#footer_action_button').text('Delete');
			$('#footer_action_button').removeClass('glyphicon-check');
			$('#footer_action_button').addClass('glyphicon-trash');
			$('.actionBtn').removeClass('btn-success');
			$('.actionBtn').addClass('btn-danger');
			$('.actionBtn').addClass('delete');
			$('.modal-title').text('Delete Post');
			$('.id').text($(this).data('id'));
			$('.deleteContent').show();
			$('.form-horizontal').hide();
			$('.title').html($(this).data('title'));
			$('#myModal').modal('show');
		});

		$('.modal-footer').on('click', '.delete', function(){
			$.ajax({
				type	: 'POST',
				url		: 'deletePost',
				data	: {
					'_token'	: $("input[name=_token]").val(),
					'id'		: $('.id').text()
				},
				success : function(data){
					$('.post' + $('.id').text()).remove();
				}
			});
		});
	</script>



</body>
</html>